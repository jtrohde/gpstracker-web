<!DOCTYPE html>
<html>
	<head>
		<title>Learning Achievement Project - 2015</title>
		<link rel="stylesheet" href="/gpstracker/global.css" />
	</head>
	<body style="margin:10px">
		<h1 style="width:100%;text-align:center">Learning Achievement Project - 2015</h1>
 		
		<h3><i>REST vs SOAP</i></h3> 
 
		<p>REST and SOAP differ in many ways, but I find the following to be most important in terms of this type of project (those perceived as pros are italicized):</p>
		<table class="data">
			<tr><th>REST</th><th>SOAP</th></tr>
			<tr>
				<td><i>Very little overhead; less verbose.</i></td>
				<td>Even for small amounts of data, some minimum amount of structure is necessary.</td>
			</tr>
			<tr>
				<td><i>URL directly references the resource being acted upon, and the action to take.<br><b>This is extremely useful when accessing a web service from Android, iOS, or Javascript, which do not include support for creating SOAP proxies, but inherently support HTTP.</b></i></td>
				<td>Object is referenced within the message (i.e., method with parameter(s)). This is simple when the proxy code can be generated easily (e.g., Java or .NET).</td>
			</tr>
			<tr>
				<td><i>Uses familiar HTTP verbs that correspond to CRUD operations on the resource.</i></td>
				<td>Requires methods to be defined.</td>
			</tr>
			<tr>
				<td><i>Payload not constrained to any particular data format. JSON is often used because it is easy to parse on any platform.</i></td>
				<td>Payload must follow SOAP schemas and be in XML format. Can be difficult to parse unless a suitable parser library is used.</td>
			</tr>
			<tr>
				<td>No standards for client-server contract; changes to service definition are not easily communicated to clients.</td>
				<td><i>Well-defined client-server contract (WSDL). Refreshing previously generated proxy code is generally a one-click operation in IDEs.</i></td>
			</tr>
		</table>

		<a href="/gpstracker/report">Back</a>
	</body>
</html>
 