<!DOCTYPE html>
<html>
	<head>
		<title>Learning Achievement Project - 2015</title>
		<link rel="stylesheet" href="/gpstracker/global.css" />
	</head>
	<body style="margin:10px">
		<h1 style="width:100%;text-align:center">Learning Achievement Project - 2015</h1>

 		<h2>Objective</h2> 
 		<p>
			Develop a system to track objects by location, consisting of: 
			<ul><li>Centralized storage of object data</li> 
			<li>A REST API to manipulate object states, which is independent of the calling platform</li>
			<li>A client application to utilize REST API methods</li></ul>
 		</p>
		<h2>System Components</h2>
 		<ul><li>LAMP stack (cloud-hosted)</li>
		<li>Object coordinate database (MySQL)</li>
		<li>Web service endpoint to handle and delegate HTTP requests (PHP)</li>
		<li>Web application to add/edit/delete objects and display their location (PHP/HTML5)</li>
		<li>Android application to add/edit/delete objects and display their location (Java)</li>
		<li>API documentation</li></ul>
 
		<h2>Learning Goals and Outcomes</h2> 
		<table class="data">
			<tr><th>Goal</th><th>Outcome</th></tr>
			<tr>
				<td>Platform-independent manipulation of centralized data</td>
				<td>
					<ul>
						<li>Refreshed existing knowledge of HTML5, MySQL, and PHP development</li>
						<li>Added knowledge of parsing and manipulating HTTP verbs and parameters</li>
					</ul>
				</td>
			</tr>
			<tr><td>REST API design and implementation</td><td>Successfully designed and implemented a <a href="/gpstracker/reference/">REST API</a></td></tr>
			<tr><td>Differences between REST and SOAP; uses, advantages and disadvantages of each</td><td><a href="restvssoap.php">See comparison</a></td></tr>
			<tr>
				<td>Google Maps integration</td>
				<td>Achieved basic knowledge of setting up Google Maps API for use in JavaScript and Android. Learned to:
					<ul>
						<li>Embed a Google Map in a web application and an Android application</li>
						<li>Initialize the map</li>
						<li>Move the map to a location</li>
						<li>Place a marker at a location on the map</li>
					</ul>
				</td>
			</tr>
		</table>
	</body>
</html>