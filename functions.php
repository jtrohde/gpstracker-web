<?php
	// Initialize server settings
	$host = "mysql.jtrohde.com";
	$username = "jtrohde";
	$password = "66A9oF%r91!";
	$db = "com_jtrohde_learningachievement";

	/**
	 * Fetch all rows from the gps_coords table.
	 */
	function get_gps_coords(&$responseText) {
		// Connect to the database 
		$conn = @mysqli_connect($GLOBALS["host"], $GLOBALS["username"], $GLOBALS["password"], $GLOBALS["db"]);
		if (!$conn) {
			$responseText = mysqli_connect_error();
			return 500;
		}

		// Fetch gps-tracked objects
		$query = "select id, name, description, latitude, longitude from gps_coords order by id";
		$result = @mysqli_query($conn, $query);
		if (!$result) {
			$responseText = mysqli_error($conn);
			return 500;
		}

		// Close connection
		mysqli_close($conn);

		// Initialize JSON array
		$json = "[";

		// Populate JSON array with individual objects
		while ($row = @mysqli_fetch_array($result)) {
			$arr = array(
				"id" => $row["id"],
				"name" => $row["name"],
				"description" => $row["description"],
				"latitude" => $row["latitude"],
				"longitude" => $row["longitude"]
			);
			$json .= json_encode($arr, JSON_FORCE_OBJECT) . ",";
		}

		// Remove trailing comma
		$json = rtrim($json, ',');	

		// Close the JSON array
		$json .= "]";

		$responseText = $json;
		return 200;
	}

	/**
	 * Insert a new record into the GPS coordinates table. 	
	 */
	function insert_gps_coords($name, $desc, $lat, $long, &$responseText) {
		// Connect to the database
		$conn = @mysqli_connect($GLOBALS["host"], $GLOBALS["username"], $GLOBALS["password"], $GLOBALS["db"]);
		if (mysqli_connect_errno()) {
			$responseText = mysqli_connect_error();
			return 500;
		}

		// Execute insert of new row
		$stmt = mysqli_prepare($conn, "insert into gps_coords (name, description, latitude, longitude) values(?,?,?,?)");
		mysqli_stmt_bind_param($stmt, "ssdd", $name, $desc, $lat, $long);
		$result = @mysqli_stmt_execute($stmt);
		if (!$result) {
			$responseText = mysqli_error($conn);
			return 500;
		}

		// Get the ID of the new row
		$id = mysqli_insert_id($conn);

		// Close connection
		mysqli_close($conn);

		$responseText = $id;
		return 201;
	}

	/**
	 * Update an existing record in the GPS coordinates table.
	 */
	function update_gps_coords($id, $name, $desc, $lat, $long, &$responseText) {
		// Connect to the database
		$conn = @mysqli_connect($GLOBALS["host"], $GLOBALS["username"], $GLOBALS["password"], $GLOBALS["db"]);
		if (mysqli_connect_errno()) {
			$responseText = mysqli_connect_error();
			return 500;
		}

		// Check for identical existing record
		$query = mysqli_prepare($conn, "select count(*) from gps_coords where id = ? and name = ? and description = ? and latitude = ? and longitude = ?");
		mysqli_stmt_bind_param($query, "dssdd", $id, $name, $desc, $lat, $long);
		$result = @mysqli_stmt_execute($query);
		mysqli_stmt_bind_result($query, $count);
		mysqli_stmt_fetch($query);

		// Close the first connection
		mysqli_close($conn);

		if ($count != "0") {
			// No need to update
			file_put_contents("out.txt", "No update to identical record.\n", FILE_APPEND);
			return 200;
		}
		else {
			file_put_contents("out.txt", "Updating record.\n", FILE_APPEND);

			// Open a new connection
			$conn = @mysqli_connect($GLOBALS["host"], $GLOBALS["username"], $GLOBALS["password"], $GLOBALS["db"]);
			if (mysqli_connect_errno()) {
				$responseText = mysqli_connect_error();
				return 500;
			}

			// Execute the update
			$stmt = mysqli_prepare($conn, "update gps_coords set name = ?, description = ?, latitude = ?, longitude = ? where id = ?");
			file_put_contents("out.txt", "Prepared update statement.\n", FILE_APPEND);
			mysqli_stmt_bind_param($stmt, "ssddd", $name, $desc, $lat, $long, $id);
			file_put_contents("out.txt", "Bound update parameters.\n", FILE_APPEND);
			$result = @mysqli_stmt_execute($stmt);
			file_put_contents("out.txt", "Executed update. Result: " . $result, FILE_APPEND);
			if (!$result) {
				file_put_contents("out.txt", "No results.\n", FILE_APPEND);
				$responseText = mysqli_error($conn);
				file_put_contents("out.txt", "Error: response text: " . $responseText . "\n", FILE_APPEND);
				return 500;
			}

			// Check for affected rows
			file_put_contents("out.txt", "Checking for affected rows.\n", FILE_APPEND);
			$affectedRows = mysqli_affected_rows($conn);
			file_put_contents("out.txt", "Affected rows: " . $affectedRows . "\n", FILE_APPEND);
			$statusCode = ($affectedRows == 0)? 404 : 200;

			// Close connection
			mysqli_close($conn);
		}


		return $statusCode;
	}

	/** 
	 * Delete the specified record from the GPS coordinates table.
	 */
	function delete_gps_coords($id, &$responseText) {
		// Connect to the database
		$conn = @mysqli_connect($GLOBALS["host"], $GLOBALS["username"], $GLOBALS["password"], $GLOBALS["db"]);
		if (mysqli_connect_errno()) {
			$responseText = mysqli_connect_error();
			return 500;
		}

		// Execute the delete
		$stmt = mysqli_prepare($conn, "delete from gps_coords where id = ?");
		mysqli_stmt_bind_param($stmt, "d", $id);
		$result = @mysqli_stmt_execute($stmt);
		if (!$result) {
			$responseText = mysqli_error($conn);
			return 500;
		}

		mysqli_close($conn);
		
		return 200;
	}

	if (!function_exists('http_response_code')) {
        function http_response_code($code = NULL) {

            if ($code !== NULL) {
                switch ($code) {
                    case 100: $text = 'Continue'; break;
                    case 101: $text = 'Switching Protocols'; break;
                    case 200: $text = 'OK'; break;
                    case 201: $text = 'Created'; break;
                    case 202: $text = 'Accepted'; break;
                    case 203: $text = 'Non-Authoritative Information'; break;
                    case 204: $text = 'No Content'; break;
                    case 205: $text = 'Reset Content'; break;
                    case 206: $text = 'Partial Content'; break;
                    case 300: $text = 'Multiple Choices'; break;
                    case 301: $text = 'Moved Permanently'; break;
                    case 302: $text = 'Moved Temporarily'; break;
                    case 303: $text = 'See Other'; break;
                    case 304: $text = 'Not Modified'; break;
                    case 305: $text = 'Use Proxy'; break;
                    case 400: $text = 'Bad Request'; break;
                    case 401: $text = 'Unauthorized'; break;
                    case 402: $text = 'Payment Required'; break;
                    case 403: $text = 'Forbidden'; break;
                    case 404: $text = 'Not Found'; break;
                    case 405: $text = 'Method Not Allowed'; break;
                    case 406: $text = 'Not Acceptable'; break;
                    case 407: $text = 'Proxy Authentication Required'; break;
                    case 408: $text = 'Request Time-out'; break;
                    case 409: $text = 'Conflict'; break;
                    case 410: $text = 'Gone'; break;
                    case 411: $text = 'Length Required'; break;
                    case 412: $text = 'Precondition Failed'; break;
                    case 413: $text = 'Request Entity Too Large'; break;
                    case 414: $text = 'Request-URI Too Large'; break;
                    case 415: $text = 'Unsupported Media Type'; break;
                    case 500: $text = 'Internal Server Error'; break;
                    case 501: $text = 'Not Implemented'; break;
                    case 502: $text = 'Bad Gateway'; break;
                    case 503: $text = 'Service Unavailable'; break;
                    case 504: $text = 'Gateway Time-out'; break;
                    case 505: $text = 'HTTP Version not supported'; break;
                    default:
                        exit('Unknown http status code "' . htmlentities($code) . '"');
                    break;
                }

                $protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');

                header($protocol . ' ' . $code . ' ' . $text);

                $GLOBALS['http_response_code'] = $code;

            } else {

                $code = (isset($GLOBALS['http_response_code']) ? $GLOBALS['http_response_code'] : 200);

            }

            return $code;

        }
    }
?>