<!DOCTYPE html>
<?php
	include_once("functions.php");

	$method = strtoupper($_SERVER["REQUEST_METHOD"]);
	$action = strtoupper($_POST["action"]);
	
	/*
	 Delegate to function for request type.

	 Note that because HTML forms only support GET and POST, we are using POST with an action code argument here,
	 in place of DELETE and PUT calls that would typically be made.
	 */
	if ($method == "POST") {
		switch ($action) {
			case "ADD":
			case "UPDATE":
				$id = $_POST["id"];
				$name = $_POST["name"];
				$desc = $_POST["desc"];
				$lat = $_POST["lat"];
				$long = $_POST["long"];

				if ($id == "0") {
					// Insert a new record
					$result = insert_gps_coords($name, $desc, $lat, $long, $response);
				}
				else {
					// Update existing record
					$result = update_gps_coords($id, $name, $desc, $lat, $long, $response);
				}
				break;
			
			case "DELETE":
				$id = $_POST["id"];

				// Delete the specified record
				$result = delete_gps_coords($id, $response);
				break;

			default:
				$result = true;
				break;
		}

		if (!$result) {
			echo $err;
		}
	}
?>
<html>
	<head>
		<title>GPS Tracking Demo</title>
		<link rel="stylesheet" type="text/css" href="global.css">
		<script src="http://maps.googleapis.com/maps/api/js"></script>
		<script>
			function validateInput() {
				if (!document.getElementById("name").value) {
					alert("Name required.");
					return false;
				}

				if (!document.getElementById("desc").value) {
					alert("Description required.");
					return false;
				}
				
				if (!document.getElementById("lat").value) {
					alert("Latitude required.");
					return false;
				}

				if (!document.getElementById("long").value) {
					alert("Longitude required.");
					return false;
				}

				return true;
			}

			function showGpsCoordinatesOnMap(name, lat, lng) {
				var center = new google.maps.LatLng(lat, lng);
				var mapProp = {
					center: center,
					zoom: 13,
					mapTypeId: google.maps.MapTypeId.HYBRID
				};

				var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

				var marker = new google.maps.Marker({
					position: center,
					map: map,
					title: name
				});

				google.maps.event.addListener(marker, 'mouseover', function() {
					infowindow.open(map, marker);
				});
			}

			function getCurrentPosition(id) {
				document.getElementById("current_location_button_" + id).innerText = "Locating...";
				
				function updateStatusText(id) {
					document.getElementById("current_location_button_" + id).innerText = "Locate";
				}

				navigator.geolocation.getCurrentPosition(function (position) {
					document.getElementById("edit_lat_" + id).value = position.coords.latitude;
					document.getElementById("edit_long_" + id).value = position.coords.longitude;
					updateStatusText(id);
				});
			}

			function editItem(id) {
				// Modify the selected row to display editable text fields and hide static labels
				var name = document.getElementById("name_" + id);
				var editName = document.getElementById("edit_name_" + id);
				var desc = document.getElementById("desc_" + id);
				var editDesc = document.getElementById("edit_desc_" + id);
				var lat = document.getElementById("lat_" + id);
				var editLat = document.getElementById("edit_lat_" + id);
				var long = document.getElementById("long_" + id);
				var editLong = document.getElementById("edit_long_" + id);
       			var editButton = document.getElementById("edit_button_" + id);
       			var saveButton = document.getElementById("save_button_" + id);
       			var currentLocationButton = document.getElementById("current_location_button_" + id);

				editName.value = name.innerText;
				editName.style.display = "inline";
				name.style.display = "none";

				editDesc.value = desc.innerText;
				editDesc.style.display = "inline";
				desc.style.display = "none";

				editLat.value = lat.innerText;
				editLat.style.display = "inline";
				lat.style.display = "none";

				editLong.value = long.innerText;
				editLong.style.display = "inline";
				long.style.display = "none";

				editButton.style.display = "none";
				saveButton.style.display = "inline";				
				currentLocationButton.style.display = "inline";
			}

			function updateItem(id) {
				if (!validateInput(id)) {
					return;
				}

				document.getElementById("action_" + id).value = "update";
				document.getElementById("form_" + id).submit();
			}

			function deleteItem(id) {
				document.getElementById("action_" + id).value = "delete";
				document.getElementById("form_" + id).submit();
			}

			function toggleNewItemEntry() {
				if (document.getElementById("add_new_item_entry").style.display == "table-row") {
					// Display edit fields
					document.getElementById("add_new_item_entry").style.display = "none";
					document.getElementById("edit_new_item_entry").style.display = "table-row";
				}
				else {
					// Hide and clear edit fields
					document.getElementById("add_new_item_entry").style.display = "table-row";
					document.getElementById("edit_new_item_entry").style.display = "none";
					document.getElementById("edit_name_0").value = "";
					document.getElementById("edit_desc_0").value = "";
					document.getElementById("edit_lat_0").value = "";
					document.getElementById("edit_long_0").value = "";
				}
			}

			function addItem() {
				if (!validateInput(0)) {
					return;
				}

				document.forms["form_0"].submit();
			}

			function validateInput(id) {
				return true;
			}
		</script>
	</head>
	<body>
		<h1 style="width:100%;text-align:center;">GPS Tracker Demo</h1>
		<div id="page-wrapper">
			<div id="left">
				<table>
					<tr>
						<td>Name</td>
						<td>Description</td>
						<td>Latitude</td>
						<td colspan="3">Longitude</td>
					</tr>
					<?php
						// List existing objects and their coordinates
						$conn = mysqli_connect($host, $username, $password, $db);
						$query = "select id, name, description, latitude, longitude from gps_coords";
						$result = mysqli_query($conn, $query);
						mysqli_close($conn);
						$i = 0;
						while ($row = mysqli_fetch_array($result)) {
							$id = $row["id"];
							$name = $row["name"];
							$desc = $row["description"];
							$lat = $row["latitude"];
							$long = $row["longitude"];
							?>
							<form id="form_<?php echo $id?>" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
								<tr class="<?php echo $i%2==0?"even":"odd";?>">
									<input type="hidden" name="id" value="<?php echo $id?>" />
									<input type="hidden" id="action_<?php echo $id?>" name="action" />
									<td><span id="name_<?php echo $id?>"><?php echo $name?></span><input type="text" id="edit_name_<?php echo $id?>" name="name" value="<?php echo $name?>" style="display:none"/></td>
									<td><span id="desc_<?php echo $id?>"><?php echo $desc?></span><input type="text" id="edit_desc_<?php echo $id?>" name="desc" value="<?php echo $desc?>" style="display:none"/></td>
									<td><span id="lat_<?php echo $id?>"><?php echo $lat?></span><input type="text" id="edit_lat_<?php echo $id?>" name="lat" value="<?php echo $lat?>" style="display:none"/></td>
									<td><span id="long_<?php echo $id?>"><?php echo $long?></span><input type="text" id="edit_long_<?php echo $id?>" name="long" value="<?php echo $long?>" style="display:none"/></td>
									<td><a href="#" id="show_coords_button" class="standard" onClick="showGpsCoordinatesOnMap(<?php echo "'$name'"?>, <?php echo $lat?>, <?php echo $long?>)">Show</a></td>
									<td><a href="#" id="delete_button" class="standard" onClick="deleteItem(<?php echo $id?>)">Delete</a></td>
									<td><a href="#" id="edit_button_<?php echo $id?>" class="standard" onClick="editItem(<?php echo $id?>)">Edit</a><a href="#" class="standard" id="save_button_<?php echo $id?>" class="standard" style="display:none" onClick="updateItem(<?php echo $id?>)">Save</a></td>
									<td><a href="#" id="current_location_button_<?php echo $id?>" class="standard" onClick="getCurrentPosition(<?php echo $id?>)" style="display:none">Locate</a></td>
								</tr>
							</form>
							<?php
							$i++;
						}
					?>

					<!-- Row to trigger new item entry row -->
					<tr id="add_new_item_entry" class="<?php echo $i++%2==0?"even":"odd"?>" style="display:table-row">
						<td colspan="8"><a href="#" class="standard" onClick="toggleNewItemEntry()">Add</a></td>
					</tr>
					<!-- Row for new item entry -->
					<form id="form_0" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
						<tr id="edit_new_item_entry" class="<?php echo $i++%2==0?"even":"odd"?>" style="display:none">
							<input type="hidden" name="id" value="0" />
							<input type="hidden" id="action_0" name="action" value="add" />
							<td><input type="text" id="edit_name_0" name="name" /></td>
							<td><input type="text" id="edit_desc_0" name="desc" /></td>
							<td><input type="text" id="edit_lat_0" name="lat" /></td>
							<td colspan="2"><input type="text" id="edit_long_0" name="long" /></td>
							<td><a href="#" id="cancel_button" class="standard" onclick="toggleNewItemEntry()">Cancel</a></td>
							<td><a href="#" id="save_button" class="standard" onclick="updateItem(0)">Save</a></td>
							<td><a href="#" id="current_location_button_0" class="standard" onClick="getCurrentPosition(0)">Locate</a></td>
						</tr>
					</form>
				</table>
			</div>
			<div id="right">
				<div id="googleMap" style="float:left;width:500px;height:380px"></div>
			</div>
		</div>
		<div style="display:block;text-align:left">
			Additional resources:
			<ul><li><a href="downloads/GpsTracker.apk">GPS Tracker Android App</a></li>
			<li><a href="reference/">API Reference</a></li>
			<li><a href="report/">Project Summary</a></li></ul>
		</div>
	</body>
</html>