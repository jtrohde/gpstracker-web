<?php
	include_once($_SERVER["DOCUMENT_ROOT"] . "/gpstracker/reference/menu_functions.php");
?>
<!DOCTYPE html>
<html>
	<head>
		<title>GET items/</title>
		<link rel="stylesheet" href="/gpstracker/global.css" />
		<link rel="stylesheet" href="/gpstracker/reference/api_reference.css" />
		<!--<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/8.6/styles/default.min.css" /> -->
		<link rel="stylesheet" href="https://highlightjs.org/static/demo/styles/androidstudio.css" />
		<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/8.6/highlight.min.js"></script>
		<script>hljs.initHighlightingOnLoad();</script>
	</head>
	<body>
		<div id="page-wrapper">
			<div id="menu">
				<?php load_menu();?>
			</div>
			<div id="content">
				<h1>GET items/</h1>
				<p>Returns a JSON array of objects containing location information.</p>
				<h2>Resource URL</h2>
				<p class="code">http://www.jtrohde.com/gpstracker/request.php/items/<id></p>
				<h2>Parameters</h2>
				<table>
					<tr><td><span class="param">None</span></td></tr>
				</table>
				<h2>Return Value</h2>
				<table class="data">
					<tr><th>HTTP Status Code</th><th>Description</th><th>Response Body</th></tr>
					<tr><td><span class="status_code">200</span></td><td>Coordinates have been returned.</td><td>JSON array of objects.</td></tr>
					<tr><td><span class="status_code">500</span></td><td>Internal server error.</td><td>Error message text (e.g., "Failed to connect to database.").</td></tr>
				</table>
				<h2>Example Request</h2>
				<div class="code">
					<p>GET</p>
					<p>http://www.jtrohde.com/gpstracker/request.php/items</p>
				</div>
				<h2>Example Response</h2>
				<pre style="overflow:auto;width:auto;"><code class="rounded"><div class="javascript">[
  {
    "id":"38",
    "name":"GD Chicago",
    "description":"Chicago, IL",
    "latitude":"41.8867700000",
    "longitude":"-87.6330550000"
  },
  {
    "id":"42",
    "name":"Here",
    "description":"I am here now",
    "latitude":"43.0357545000",
    "longitude":"-88.1833368000"
  },
  {
    "id":"43",
    "name":"GD Savannah",
    "description":"Savannah, GA",
    "latitude":"32.0872380000",
    "longitude":"-81.1109710000"
  }
]</div></code></pre>
			</div>
		</div>
	</body>
</html>