<?php
	include_once($_SERVER["DOCUMENT_ROOT"] . "/gpstracker/reference/menu_functions.php");
?>
<!DOCTYPE html>
<html>
	<head>
		<title>POST</title>
		<link rel="stylesheet" href="/gpstracker/global.css" />
		<link rel="stylesheet" href="/gpstracker/reference/api_reference.css" />
		<!-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/8.6/styles/default.min.css" /> -->
		<link rel="stylesheet" href="https://highlightjs.org/static/demo/styles/androidstudio.css" />
		<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/8.6/highlight.min.js"></script>
		<script>hljs.initHighlightingOnLoad();</script>
	</head>
	<body>
		<div id="page-wrapper">
			<div id="menu">
				<?php load_menu();?>
			</div>
			<div id="content">
				<h1>POST /</h1>
				<p>Creates a new resource.</p>
				<p>If successful, returns the new ID of the resource.</p>
				<h2>Resource URL</h2>
				<p class="code">http://www.jtrohde.com/gpstracker/request.php/<id></p>
				<h2>Parameters</h2>
				<table>
					<tr><td><span class="param">name</span></td><td>Object name</td></tr>
					<tr><td><span class="param">description</span></td><td>Object description</td></tr>
					<tr><td><span class="param">latitude</span></td><td>Object latitude</td></tr>
					<tr><td><span class="param">longitude</span></td><td>Object longitude</td></tr>
				</table>
				<h2>Return Value</h2>
				<table class="data">
					<tr><th>HTTP Status Code</th><th>Description</th><th>Response Body</th></tr>
					<tr><td><span class="status_code">201</span></td><td>The resource was created successfully.</td><td>The ID of the newly-created resource.</td></tr>
					<tr><td><span class="status_code">500</span></td><td>Internal server error.</td><td>Error message text (e.g., "Failed to connect to database.").</td></tr>
				</table>
				<h2>Example Request</h2>
				<div class="code">
					<p>POST</p>
					<p>http://www.jtrohde.com/gpstracker/request.php/</p>
				</div>
				<h2>Example Response</h2>
				<pre style="overflow:auto;width:auto;"><code class="rounded"><div class="javascript">67</div></code></pre>			
			</div>
		</div>
	</body>
</html>