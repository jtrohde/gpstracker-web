<?php
	include_once($_SERVER["DOCUMENT_ROOT"] . "/gpstracker/reference/menu_functions.php");
?>
<!DOCTYPE html>
<html>
	<head>
		<title>API Reference - GPS Tracker</title>
		<link rel="stylesheet" href="/gpstracker/global.css" />
		<link rel="stylesheet" href="/gpstracker/reference/api_reference.css" />
		<!-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/8.6/styles/default.min.css" /> -->
		<link rel="stylesheet" href="https://highlightjs.org/static/demo/styles/androidstudio.css" />
		<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/8.6/highlight.min.js"></script>
		<script>hljs.initHighlightingOnLoad();</script>
	</head>
	<body>
		<div id="page-wrapper">
			<div id="menu">
				<?php load_menu();?>
			</div>
			<div id="content">
				<h1>GPS Tracker API Reference</h1><p>Please select an API from the menu.</p>
			</div>
		</div>
	</body>
</html>