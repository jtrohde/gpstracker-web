<?php
	function load_menu() {
		print "<h2><a href='/gpstracker/reference/'>API Reference</a></h2>";
		print "<ul>";
		print "<li><a href='/gpstracker/reference/get/items/'>GET items/</a></li>";
		print "<li><a href='/gpstracker/reference/put/items/id'>PUT items/id/:id</a></li>";
		print "<li><a href='/gpstracker/reference/post/'>POST items/</a></li>";
		print "<li><a href='/gpstracker/reference/delete/items/id'>DELETE items/id/:id</a></li>";
		print "</ul>";
		print "<p><a href='/gpstracker'>GPS Tracker Home</a></p>";
	}
?>