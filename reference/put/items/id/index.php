<?php
	include_once($_SERVER["DOCUMENT_ROOT"] . "/gpstracker/reference/menu_functions.php");
?>
<!DOCTYPE html>
<html>
	<head>
		<title>PUT items/</title>
		<link rel="stylesheet" href="/gpstracker/global.css" />
		<link rel="stylesheet" href="/gpstracker/reference/api_reference.css" />
		<!-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/8.6/styles/default.min.css" /> -->
		<link rel="stylesheet" href="https://highlightjs.org/static/demo/styles/androidstudio.css" />
		<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/8.6/highlight.min.js"></script>
		<script>hljs.initHighlightingOnLoad();</script>
	</head>
	<body>
		<div id="page-wrapper">
			<div id="menu">
				<?php load_menu();?>
			</div>
			<div id="content">
				<h1>PUT items/id/:id</h1>
				<p>Updates an existing resource.</p>
				<h2>Resource URL</h2>
				<p class="code">http://www.jtrohde.com/gpstracker/request.php/items/id/:id<id></p>
				<h2>Parameters</h2>
				<table>
					<tr><td><span class="param">id</span></td><td>The ID of the object to update</td></tr>
					<tr><td><span class="param">name</span></td><td>The new object name</td></tr>
					<tr><td><span class="param">description</span></td><td>The new object description</td></tr>
					<tr><td><span class="param">latitude</span></td><td>The new latitude</td></tr>
					<tr><td><span class="param">longitude</span></td><td>The new longitude</td></tr>
				</table>
				<h2>Return Value</h2>
				<table class="data">
					<tr><th>HTTP Status Code</th><th>Description</th><th>Response Body</th></tr>
					<tr><td><span class="status_code">200</span></td><td>The resource was updated successfully.</td><td>None</td></tr>
					<tr><td><span class="status_code">404</span></td><td>No resource with the specified ID was found.</td><td>None</td></tr>
					<tr><td><span class="status_code">500</span></td><td>Internal server error.</td><td>Error message text (e.g., "Failed to connect to database.").</td></tr>
				</table>
				<h2>Example Request</h2>
				<div class="code">
					<p>PUT</p>
					<p>http://www.jtrohde.com/gpstracker/request.php/items/id/67?name=test&description=new%20%object&latitude=41.88&longitude=-88.0567</p>
				</div>	
			</div>
		</div>
	</body>
</html>