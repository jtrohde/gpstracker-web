<?php
	include_once("functions.php");

	// Check for post data
	if (empty($_POST)) {
		print "No data recieved.";
	}
	else {
		if ($_POST["action"] == "insert") {
			$name = $_POST["name"];
			$desc = $_POST["desc"];
			$lat = $_POST["lat"];
			$long = $_POST["long"];

			// Insert a new record
			$result = insert_gps_coords($name, $desc, $lat, $long);
		}
		elseif ($_POST["action"] == "delete") {
			$id = $_POST["id"];

			// Delete the specified record
			$result = delete_gps_coords($id);
		}

		print $result;
	}
?>