<?php
	// Get DB connection values
	include_once("functions.php");

	// Get path info and arguments 
	$info = $_SERVER["PATH_INFO"];
	$info_parts = explode("/", $info);
	$query = parse_url($_SERVER["REQUEST_URI"], PHP_URL_QUERY);
	parse_str($query, $query_params);

	header('Access-Control-Allow-Origin: *');  

	// Delegate to function based on request type
	switch ($_SERVER["REQUEST_METHOD"]) {
		case "GET":
			/* Return an array of all existing items */
			if ($info_parts[1] == "items") {
				$responseCode = get_gps_coords($responseText);
				http_response_code($responseCode);
				print $responseText;
			}
			else {
				http_response_code(404);
			}
			break;

		case "POST":
			// Fetch arguments
			$id	= $_POST["id"];
			$name = $_POST["name"];
			$description = $_POST["description"];
			$latitude = $_POST["latitude"];
			$longitude = $_POST["longitude"];
			$method = $_POST["method"];

			if ($method == "PUT") {
				if ($id) {
					// Update the specified record
					$responseCode = update_gps_coords($id, $name, $description, $latitude, $longitude, $responseText);
					http_response_code($responseCode);
					print $responseText;
				}
				else {
					http_response_code(404);
				}
			}
			else if ($method == "DELETE") {
				if ($id) {
					// Delete item
					$responseCode = delete_gps_coords($id, $responseText);
					http_response_code($responseCode);
					print $responseText;
				}
				else {
					http_response_code(404);
				}
			}
			else {
				// Insert a new record, returning the assigned ID
				$responseCode = insert_gps_coords($name, $description, $latitude, $longitude, $responseText);
				http_response_code($responseCode);
				print $responseText;
			}
			break;

		case "PUT":
			/* Update the existing resource */

			if ($info_parts[1] == "items" && $info_parts[2] = "id") {
				$id	= $info_parts[3];
				if ($id) {
					// Fetch arguments
					$name = $query_params["name"];
					$description = $query_params["description"];
					$latitude = $query_params["latitude"];
					$longitude = $query_params["longitude"];

					// Update the specified record
					$responseCode = update_gps_coords($id, $name, $description, $latitude, $longitude, $responseText);
					http_response_code($responseCode);
					print $responseText;
				}
				else {
					http_response_code(404);
				}
			}
			else {
				http_response_code(404);
			}
			break;

		case "DELETE":
			// Delete the existing resource
			if ($info_parts[1] == "items" && $info_parts[2] = "id") {
				$id	= $info_parts[3];
				if ($id) {
					// Delete item
					$responseCode = delete_gps_coords($id, $responseText);
					http_response_code($responseCode);
					print $responseText;
				}
			}
			break;			
	}
?>
