<!DOCTYPE html>
<html>
	<head>
		<title>Location Tracking Demo</title>
		<link rel="stylesheet" type="text/css" href="global.css">
		<script src="http://maps.googleapis.com/maps/api/js"></script>
		<script>
			function validateInput() {
				if (!document.getElementById("name").value) {
					alert("Name required.");
					return false;
				}

				if (!document.getElementById("desc").value) {
					alert("Description required.");
					return false;
				}
				
				if (!document.getElementById("lat").value) {
					alert("Latitude required.");
					return false;
				}

				if (!document.getElementById("long").value) {
					alert("Longitude required.");
					return false;
				}

				return true;
			}

			function showCoordinatesOnMap(name, lat, lng) {
				var center = new google.maps.LatLng(lat, lng);
				var mapProp = {
					center: center,
					zoom: 13,
					mapTypeId: google.maps.MapTypeId.HYBRID
				};

				var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

				var marker = new google.maps.Marker({
					position: center,
					map: map,
					title: name
				});

				google.maps.event.addListener(marker, 'mouseover', function() {
					infowindow.open(map, marker);
				});
			}

			function getCurrentPosition(id) {
				document.getElementById("current_location_button_" + id).innerHTML = "Locating...";
				
				function updateStatusText(id) {
					document.getElementById("current_location_button_" + id).innerHTML = "Locate";
				}

				navigator.geolocation.getCurrentPosition(function (position) {
					document.getElementById("edit_lat_" + id).value = position.coords.latitude;
					document.getElementById("edit_long_" + id).value = position.coords.longitude;
					updateStatusText(id);
				});
			}

			function editItem(id) {
				// Modify the selected row to display editable text fields and hide static labels
				var name = document.getElementById("name_" + id);
				var editName = document.getElementById("edit_name_" + id);
				var desc = document.getElementById("desc_" + id);
				var editDesc = document.getElementById("edit_desc_" + id);
				var lat = document.getElementById("lat_" + id);
				var editLat = document.getElementById("edit_lat_" + id);
				var long = document.getElementById("long_" + id);
				var editLong = document.getElementById("edit_long_" + id);
				var editButton = document.getElementById("edit_button_" + id);
				var saveButton = document.getElementById("save_button_" + id);
				var currentLocationButton = document.getElementById("current_location_button_" + id);

				editName.value = name.innerHTML;
				editName.style.display = "inline";
				name.style.display = "none";

				editDesc.value = desc.innerHTML;
				editDesc.style.display = "inline";
				desc.style.display = "none";

				editLat.value = lat.innerHTML;
				editLat.style.display = "inline";
				lat.style.display = "none";

				editLong.value = long.innerHTML;
				editLong.style.display = "inline";
				long.style.display = "none";

				editButton.style.display = "none";
				saveButton.style.display = "inline";				
				currentLocationButton.style.display = "inline";
			}

			function getItems() {
				var request = new XMLHttpRequest();
				request.onreadystatechange = function() {
  					if (request.readyState == 4 && request.status == 200) {
						listObjects(request.responseText);
    				}
  				};
				request.open("GET", "http://jtrohde.com/gpstracker/request.php/items");
				request.send();				
			}

			function updateItem(id) {
				if (!validateInput(id)) {
					return;
				}

				var name = document.getElementById("edit_name_" + id).value;
				var description = document.getElementById("edit_desc_" + id).value;
				var latitude = document.getElementById("edit_lat_" + id).value;
				var longitude = document.getElementById("edit_long_" + id).value;
				var params =  "id=" + id +  
					"&name=" + encodeURIComponent(name) + 
					"&description=" + encodeURIComponent(description) + 
					"&latitude=" + latitude +
					"&longitude=" + longitude;
				if (id != "0") {
					params += "&method=PUT";
				}

				var request = new XMLHttpRequest();
				request.onreadystatechange = function() {
						if (request.readyState == 4) {
							switch (request.status) {
								case 200:
									getItems();
									break;
								case 201:
									getItems();
									document.getElementById("edit_new_item_entry").style.display = "none";
									document.getElementById("add_new_item_entry").style.display = "inline";
									break;
								break;
							case 404:
							case 500:
								alert(request.responseText);
								break;
						}
					}
				};
				request.open("POST", "http://jtrohde.com/gpstracker/request.php", true);
  				request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
				request.send(params);
			}

			function deleteItem(id) {
				var params = "method=DELETE&id=" + id;
				var request = new XMLHttpRequest();
				request.onreadystatechange = function() {
						if (request.readyState == 4) {
							switch (request.status) {
								case 200:
								getItems();
								break;
							case 404:
							case 500:
								alert(request.responseText);
								break;
						}	
					}
				};
				request.open("POST", "http://jtrohde.com/gpstracker/request.php");
  				request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
				request.send(params);
			}

			function toggleNewItemEntry() {
				if (document.getElementById("add_new_item_entry").style.display == "table-row") {
					// Display edit fields
					document.getElementById("add_new_item_entry").style.display = "none";
					document.getElementById("edit_new_item_entry").style.display = "table-row";
				}
				else {
					// Hide and clear edit fields
					document.getElementById("add_new_item_entry").style.display = "table-row";
					document.getElementById("edit_new_item_entry").style.display = "none";
					document.getElementById("edit_name_0").value = "";
					document.getElementById("edit_desc_0").value = "";
					document.getElementById("edit_lat_0").value = "";
					document.getElementById("edit_long_0").value = "";
				}
			}

			function validateInput(id) {
				return true;
			}

			function listObjects(text) {
				var table = document.getElementById("list");

				// Remove existing data rows (always leave last 2 rows for new entry)
				while (table.rows.length > 2) {
					table.deleteRow(0);
				}

				// Parse JSON-encoded string into iterable object
				var objects = JSON.parse(text);

				// Put new row for each object
				for (var i=0; i<objects.length; i++) {
					var obj = objects[i];
					var row = table.insertRow(i);
					row.setAttribute("class", i%2==0?"even":"odd");
					
					row.innerHTML = "<input type='hidden' name='id' value='" + obj.id + "' />" + 
						"<input type='hidden' id='action_" + obj.id + "'>";

					var nameCell = row.insertCell(-1);
					nameCell.innerHTML = "<span id='name_" + obj.id + "'>" + obj.name + "</span>" + 
						"<input type='text' id='edit_name_" + obj.id + "' name='name' value='" + obj.name + "' style='display:none'>";

					var descCell = row.insertCell(-1);
					descCell.innerHTML = "<span id='desc_" + obj.id + "'>" + obj.description + "</span>" + 
						"<input type='text' id='edit_desc_" + obj.id + "' name='description' value='" + obj.description + "' style='display:none'>";

					var latCell = row.insertCell(-1);
					latCell.innerHTML = "<span id='lat_" + obj.id + "'>" + obj.latitude + "</span>" + 
						"<input type='text' id='edit_lat_" + obj.id + "' name='lat' value='" + obj.latitude + "' style='display:none'>";

					var longCell = row.insertCell(-1);
					longCell.innerHTML = "<span id='long_" + obj.id + "'>" + obj.longitude + "</span>" + 
						"<input type='text' id='edit_long_" + obj.id + "' name='long' value='" + obj.longitude + "' style='display:none'>";

					var showCell = row.insertCell(-1);
					showCell.innerHTML = "<a href='#'' id='show_coords_button' class='standard' onClick=\"showCoordinatesOnMap('" + obj.name + "'," + obj.latitude + "," + obj.longitude + ")\">Show</a>";

					var deleteCell = row.insertCell(-1);
					deleteCell.innerHTML = "<a href='#' id='delete_button' class='standard' onClick=\"deleteItem(" + obj.id + ")\">Delete</a>";

					var editCell = row.insertCell(-1);
					editCell.innerHTML = "<a href='#' class='standard' id='edit_button_" + obj.id + "' onClick=\"editItem(" + obj.id + ")\">Edit</a>" + 
										 "<a href='#' class='standard' id='save_button_" + obj.id + "' style='display:none' onClick=\"updateItem(" + obj.id + ")\">Save</a>";

					var locateCell = row.insertCell(-1);
					locateCell.innerHTML = "<a href='#' id='current_location_button_" + obj.id + "' class='standard' onClick=\"getCurrentPosition(" + obj.id + ")\" style='display:none'>Locate</a>";
				}

			}
		</script>
	</head>
	<body onload="getItems()">
		<h1 style="width:100%;text-align:center;">Location Tracking Demo</h1>
		<div id="page-wrapper">
			<div id="left">
				<h3>Tracked Items</h3>
				<table id="list">
					<tr>
						<td>Name</td>
						<td>Description</td>
						<td>Latitude</td>
						<td colspan="3">Longitude</td>
					</tr>
					
					<!-- Row to trigger new item entry row -->
					<tr id="add_new_item_entry" class="trigger" style="display:table-row">
						<td colspan="8"><a href="#" class="standard" onClick="toggleNewItemEntry()">Add</a></td>
					</tr>
					<!-- Row for new item entry -->
					<form id="form_0" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
						<tr id="edit_new_item_entry" class="last" style="display:none">
							<input type="hidden" name="id" value="0" />
							<input type="hidden" id="action_0" name="action" value="add" />
							<td><input type="text" id="edit_name_0" name="name" /></td>
							<td><input type="text" id="edit_desc_0" name="desc" /></td>
							<td><input type="text" id="edit_lat_0" name="lat" /></td>
							<td colspan="2"><input type="text" id="edit_long_0" name="long" /></td>
							<td><a href="#" id="cancel_button" class="standard" onclick="toggleNewItemEntry()">Cancel</a></td>
							<td><a href="#" id="save_button" class="standard" onclick="updateItem(0)">Save</a></td>
							<td><a href="#" id="current_location_button_0" class="standard" onClick="getCurrentPosition(0)">Locate</a></td>
						</tr>
					</form>
				</table>
				<h3>Additional Resources</h3>
				<ul>
					<li><a href="downloads/GpsTracker.apk">GPS Tracker Android App</a></li>
					<li><a href="reference/">API Reference</a></li>
					<li><a href="report/">Project Summary</a></li>
					<li>Sources:
						<ul>
							<li><a href="https://bitbucket.org/jtrohde/gpstracker-android/">Android</a></li>
							<li><a href="https://bitbucket.org/jtrohde/gpstracker-web/">Web App</a></li>
						</ul>
					</li>
				</ul>
			</div>
			<div id="right">
				<div id="googleMap" style="float:right;vertical-align:top;width:500px;height:380px"></div>
			</div>
		</div>	
	</body>
</html>